from django.db import models

# Create your models here.

class Peserta(models.Model):
    name = models.TextField(max_length=100, unique=True)

    def __str__(self):
        return self.name

class Kegiatan(models.Model):
    name = models.TextField(max_length=100, unique=True)
    peserta = models.ManyToManyField(Peserta, blank=True)

    def __str__(self):
        return self.name