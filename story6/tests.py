from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index, add_peserta, add_kegiatan
from .models import Peserta, Kegiatan

# Create your tests here.


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("story6:index")
        self.kegiatan = Kegiatan.objects.create(name="K-one")
        self.pesertalama = Peserta.objects.create(name="pesertalama")
        self.add_peserta_url = reverse(
            "story6:add_peserta", args=[self.kegiatan.id])
        self.add_kegiatan_url = reverse("story6:add-kegiatan")
        self.pesertadeleted = Peserta.objects.create(name="tobedeleted")
        self.delete_peserta_url = reverse("story6:delete_peserta")

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")

    def test_add_peserta_POST(self):
        response = self.client.post(self.add_peserta_url, {"name": "test"})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.kegiatan.peserta.all()[0].name, "test")

    def test_add_peserta_POST_name_ada(self):
        response = self.client.post(self.add_peserta_url, {
                                    "name": "pesertalama"})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.kegiatan.peserta.all()[0].name, "pesertalama")

    def test_add_peserta_POST_same_name(self):
        self.client.post(self.add_peserta_url, {"name": "peserta"})
        response = self.client.post(self.add_peserta_url, {"name": "peserta"})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(len(self.kegiatan.peserta.all()), 1)

    def test_add_kegiatan_GET(self):
        response = self.client.get(self.add_kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "add-kegiatan.html")

    def test_add_kegiatan_POST(self):
        response = self.client.post(self.add_kegiatan_url, {
                                    "name": "nama kegiatan"})
        self.assertEquals(response.status_code, 302)

    def test_delete_peserta_POST(self):
        # add peserta
        self.kegiatan.peserta.add(self.pesertadeleted)
        old_length = len(self.kegiatan.peserta.all())

        # delete peserta
        response = self.client.post(self.delete_peserta_url, {
                                    "name": "tobedeleted", "kegiatan_id": self.kegiatan.id})
        self.assertEquals(len(self.kegiatan.peserta.all()), old_length - 1)
        self.assertEquals(response.status_code, 302)


class TestModels(TestCase):
    def setUp(self):
        self.peserta_test = Peserta.objects.create(name="peserta")
        self.kegiatan_test = Kegiatan.objects.create(name="K-one")
        self.kegiatan_test.peserta.add(self.peserta_test)

    def test_nama_peserta(self):
        self.assertEquals(str(self.peserta_test), "peserta")

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test), "K-one")
