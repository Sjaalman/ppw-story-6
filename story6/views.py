from django.shortcuts import render, redirect, reverse
from .models import Peserta, Kegiatan
from .forms import PesertaForm, KegiatanForm

# Create your views here.
def index(request):
    context = {
        "kegiatans" : Kegiatan.objects.all(),
        "pesertaform" : PesertaForm,
    }
    return render(request, 'index.html', context)

def add_peserta(request,id):
    if request.method == "POST":
        kegiatan = Kegiatan.objects.get(id=id)
        nama = request.POST["name"]
        pesertabaru, baru = Peserta.objects.get_or_create(name=nama)
        
        if baru:
            pesertabaru.save()
            kegiatan.peserta.add(pesertabaru)
        else:
            try:
                kegiatan.peserta.get(name=nama)
            except:
                kegiatan.peserta.add(pesertabaru)

    return redirect(reverse("story6:index"))

def delete_peserta(request):
    if request.method == "POST":
        
        peserta = Peserta.objects.get(name = request.POST["name"])
        kegiatan = Kegiatan.objects.get(id = request.POST["kegiatan_id"])

        kegiatan.peserta.remove(peserta)

    return redirect("story6:index")

def add_kegiatan(request):
    if request.method == "POST":
        form_kegiatan = KegiatanForm(request.POST)

        if form_kegiatan.is_valid():
            form_kegiatan.save()

        return redirect(reverse("story6:index"))

    context = {
        "form_kegiatan" : KegiatanForm,
    }
    return render(request,"add-kegiatan.html",context)




