from django import forms
from .models import Kegiatan, Peserta

class PesertaForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control",
        "placeholder" : "Nama Peserta"
    }), label = "")
    class Meta:
        model = Peserta
        fields = "__all__"

class KegiatanForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control w-50 text-center",
        "placeholder" : "Nama Kegiatan"
    }), label = "")
    class Meta:
        model = Kegiatan
        fields = [ 'name' ]
