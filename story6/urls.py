from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path("add-peserta/<int:id>",views.add_peserta,name="add_peserta"),
    path("delete-peserta/",views.delete_peserta, name="delete_peserta"),
    path("add-kegiatan/",views.add_kegiatan, name="add-kegiatan"),
]